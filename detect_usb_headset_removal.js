import xapi from 'xapi';

let startTime = -1; // minutes
let timeLeft = 0; // seconds
let timer = 0;
var volume_level = 0;

const Sound = 'Alert';

xapi.Status.Audio.Volume.on(vol_level => {
  console.log(vol_level)
  volume_level = vol_level
});

const EndMessages = [
  'A Policy Voilation has occured ',
  'You did not plug in the usb headset within the 30 second period',
  'Internal Security has been notified',
];

function showAlert(title, text, duration = 5) {
  xapi.command('UserInterface Message Alert Display', {
    Title: title,
    Text: text,
    Duration: duration,
  });
}

xapi.Status.Audio.Devices.HeadsetUSB.ConnectionStatus.on(value => {
  console.log(value)
  if (value == "NotConnected") {
    showAlert('Compliance Alert', 'USB headsets are required by policy, an alert as been generated, please note you have 1 minutes to reconnect the usb headset.');
    start(1)
  } else if (value == "Connected") {

    cancel();
    showAlert('', 'Thank you for being a good corporate citizen');
  }
});

function showMsg(text, duration = 5) {
  xapi.command('UserInterface Message TextLine Display', {
    Text: text,
    Duration: duration,
    X: 5000,
    Y: 5000
  });
}

function clearScreen() {
  xapi.command('UserInterface Message TextLine Clear');
}

function formatTime(time) {
  let min = Math.floor(time / 60);
  if (min < 10) min = `0${min}`;
  let sec = time % 60;
  if (sec < 10) sec = `0${sec}`;
  return `${min}:${sec}`;
}

function playSound() {
  console.log(volume_level)
  if (volume_level < 50) {
    xapi.Command.Audio.Volume.Set({ Device: "Internal", Level: 50 });
  }
  xapi.command('Audio Sound Play', { Sound });
  setTimeout(() => xapi.command('Audio Sound Stop'), 128000);
}

function stopTimer() {
  clearTimeout(timer);
  timer = 0;
}

function timerFinished() {
  cancel();
  playSound();
  setTimeout(() => playSound(), 2000); // play twice
  const msg = EndMessages[ Math.floor(Math.random() * EndMessages.length) ];
  showAlert('00:00', msg, 30, 2);
}

function showTime() {
  const text = `Time left to reconnect USB Headset: ${formatTime(timeLeft)} s`;
  showMsg(text, 2);
}

function tick() {

  if (timeLeft < 0) timerFinished();
  else{
    timer = setTimeout(tick, 1000);
    showTime();
  }
  timeLeft--;
}

function startTimer() {
  showTime();
  tick();
}

function start(minutes) {
  cancel();
  startTime = Math.max(1, parseInt(minutes));
  timeLeft = startTime * 60;
  startTimer();
}

function cancel() {
  console.log('cancel');
  clearScreen();
  stopTimer();
  startTime = -1;
}



